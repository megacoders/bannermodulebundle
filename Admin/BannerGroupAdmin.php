<?php

namespace Megacoders\BannerModuleBundle\Admin;

use Megacoders\AdminBundle\Admin\BaseAdmin;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BannerGroupAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'content/banner-groups';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * {@inheritdoc}
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $subjectId = $this->id($this->getSubject());

        if ($subjectId) {
            $menu->addChild('admin.labels.banner_group', [
                'route' => 'admin_megacoders_bannermodule_bannergroup_edit',
                'routeParameters' => ['id' => $subjectId]
            ]);

            $menu->addChild('admin.labels.banners', [
                'route' => 'admin_megacoders_bannermodule_bannergroup_banner_list',
                'routeParameters' => ['id' => $subjectId]
            ]);

            if ($childAdmin !== null) {
                $menu['admin.labels.banners']->setCurrent(true);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.banner_group.name'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => [
                    'banners' => [
                        'template' => 'MegacodersBannerModuleBundle:Admin/BannerGroup:list__action__banners.html.twig'
                    ],
                    'edit' => [],
                    'delete' => []
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $formMapper
            ->with('admin.labels.banner_group')
                ->add('name', null, ['label' => 'admin.entities.banner_group.name'])
            ->end()
        ;
    }
}
