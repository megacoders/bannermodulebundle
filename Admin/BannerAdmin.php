<?php

namespace Megacoders\BannerModuleBundle\Admin;

use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\AdminBundle\Form\Type\ReadonlyType;
use Megacoders\AdminBundle\Form\Type\UploadedImageType;
use Megacoders\BannerModuleBundle\Entity\Banner;
use Megacoders\PageBundle\Form\Type\PageChoiceType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BannerAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'banners';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'sortOrder',
    );

    /**
     * @var string
     */
    protected $parentAssociationMapping = 'group';

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('startDate',
                null, ['label' => 'admin.entities.banner.start_date'],
                'sonata_type_datetime_picker', ['format' => 'yyyy-MM-dd HH:mm']
            )
            ->add('endDate',
                null, ['label' => 'admin.entities.banner.end_date'],
                'sonata_type_datetime_picker', ['format' => 'yyyy-MM-dd HH:mm']
            )
            ->add('active', null, ['label' => 'admin.entities.banner.active'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.banner.name'])
            ->add('startDate', null, [
                'label' => 'admin.entities.banner.start_date',
                'pattern' => 'yyyy-MM-dd HH:mm'
            ])
            ->add('endDate', null, [
                'label' => 'admin.entities.banner.end_date',
                'pattern' => 'yyyy-MM-dd HH:mm'
            ])
            ->add('numberOfViews', null, ['label' => 'admin.entities.banner.number_of_views_short'])
            ->add('numberOfClicks', null, ['label' => 'admin.entities.banner.number_of_clicks_short'])
            ->add('sortOrder', null, ['label' => 'admin.entities.banner.sort_order'])
            ->add('active', null, ['label' => 'admin.entities.banner.active'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $formMapper
            ->with('admin.labels.banner')
                ->add('name', null, ['label' => 'admin.entities.banner.name'])
                ->add('html', null, ['label' => 'admin.entities.banner.html'])
                ->add('image', UploadedImageType::class, [
                    'label' => 'admin.entities.banner.image',
                    'required' => false,
                    'upload_directory' => $container->getParameter('megacoders_banner.upload_directory'),
                    'public_directory' => $container->getParameter('megacoders_banner.public_directory')
                ])
                ->add('linkType', 'sonata_type_choice_field_mask', [
                    'label' => 'admin.entities.banner.link_type',
                    'choices' => array_flip(Banner::LINK_TYPES_NAMES),
                    'map' => [
                        Banner::LINK_TYPE_PAGE => ['page'],
                        Banner::LINK_TYPE_URL => ['url']
                    ]
                ])
                ->add('page', PageChoiceType::class, ['label' => false, 'required' => false])
                ->add('url', null, ['label' => false, 'required' => false])
                ->add('startDate', 'sonata_type_datetime_picker', [
                    'label' => 'admin.entities.banner.start_date',
                    'format' => 'yyyy-MM-dd HH:mm'
                ])
                ->add('endDate', 'sonata_type_datetime_picker', [
                    'label' => 'admin.entities.banner.end_date',
                    'format' => 'yyyy-MM-dd HH:mm'
                ])
                ->add('active', null, ['label' => 'admin.entities.banner.active'])
        ;

        if ($this->isEditMode()) {
            $formMapper
                    ->add('sortOrder', null, ['label' => 'admin.entities.banner.sort_order'])
                ->end()
                ->with('admin.entities.banner.statistics')
                    ->add('numberOfViews', ReadonlyType::class, ['label' => 'admin.entities.banner.number_of_views'])
                    ->add('numberOfClicks', ReadonlyType::class, ['label' => 'admin.entities.banner.number_of_clicks'])
            ;

        } else {
            $order = $this->getEntityRepository(Banner::class)
                ->createQueryBuilder('b')
                    ->select('max(b.sortOrder)')
                    ->getQuery()
                        ->getSingleScalarResult()
            ;

            $formMapper->add('sortOrder', null, [
                'label' => 'admin.entities.banner.sort_order',
                'data' => $order + 10
            ]);
        }

        $formMapper->end();
    }
}
