<?php

namespace Megacoders\BannerModuleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="banner_group")
 */
class BannerGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Banner", mappedBy="group", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder" = "ASC"})
     * @var ArrayCollection
     */
    private $banners;

    /**
     * BannerGroup constructor.
     */
    public function __construct()
    {
        $this->banners = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BannerGroup
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ArrayCollection|Banner[]
     */
    public function getBanners()
    {
        return $this->banners;
    }

    /**
     * @param ArrayCollection|Banner[] $banners
     * @return BannerGroup
     */
    public function setBanners($banners)
    {
        $this->banners->clear();

        foreach ($banners as $banner) {
            $this->addBanner($banner);
        }

        return $this;
    }

    /**
     * @param Banner $banner
     * @return BannerGroup
     */
    public function addBanner(Banner $banner)
    {
        if (!$this->banners->contains($banner)) {
            $this->banners->add($banner);
        }

        return $this;
    }

    /**
     * @param Banner $banner
     * @return BannerGroup
     */
    public function removeBanner(Banner $banner)
    {
        $this->banners->removeElement($banner);
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }
}
