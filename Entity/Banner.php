<?php

namespace Megacoders\BannerModuleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\AdminBundle\Entity\ListEntityInterface;
use Megacoders\PageBundle\Entity\Page;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="banner")
 */
class Banner implements ListEntityInterface
{
    const LINK_TYPE_PAGE = 'PAGE';

    const LINK_TYPE_URL = 'URL';

    const LINK_TYPES_NAMES = [
        Banner::LINK_TYPE_PAGE => 'admin.entities.banner.link_type_page',
        Banner::LINK_TYPE_URL => 'admin.entities.banner.link_type_url'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BannerGroup", inversedBy="banners")
     * @Assert\NotBlank()
     * @var BannerGroup
     */
    private $group;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $html;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $image;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $linkType;

    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\PageBundle\Entity\Page")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @var Page
     */
    private $page;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @var \DateTime
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @var \DateTime
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $active = true;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @var int
     */
    private $sortOrder;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $numberOfViews = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $numberOfClicks = 0;

    /**
     * @var string
     */
    private $clickLink;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Banner
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return BannerGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param BannerGroup $group
     * @return Banner
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param string $html
     * @return Banner
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Banner
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkType()
    {
        return $this->linkType;
    }

    /**
     * @return string|null
     */
    public function getLinkTypeName()
    {
        $types = self::LINK_TYPES_NAMES;

        if (isset($types[$this->getLinkType()])) {
            return $types[$this->getLinkType()];
        }

        return null;
    }

    /**
     * @param string $linkType
     * @return Banner
     */
    public function setLinkType($linkType)
    {
        $this->linkType = $linkType;
        return $this;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param Page $page
     * @return Banner
     */
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Banner
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     * @return Banner
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Banner
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Banner
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     * @return Banner
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberOfViews()
    {
        return $this->numberOfViews;
    }

    /**
     * @param mixed $numberOfViews
     * @return Banner
     */
    public function setNumberOfViews($numberOfViews)
    {
        $this->numberOfViews = $numberOfViews;
        return $this;
    }

    /**
     * @return Banner
     */
    public function incrementNumberOfViews()
    {
        $this->numberOfViews += 1;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfClicks()
    {
        return $this->numberOfClicks;
    }

    /**
     * @param int $numberOfClicks
     * @return Banner
     */
    public function setNumberOfClicks($numberOfClicks)
    {
        $this->numberOfClicks = $numberOfClicks;
        return $this;
    }

    /**
     * @return Banner
     */
    public function incrementNumberOfClicks()
    {
        $this->numberOfClicks += 1;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @return string
     */
    public function getLink() {
        if ($this->linkType == self::LINK_TYPE_PAGE && $this->page !== null) {
            return $this->page->getUrl();
        }

        return $this->url;
    }

    /**
     * @return string
     */
    public function getClickLink()
    {
        return $this->clickLink;
    }

    /**
     * @param string $clickLink
     * @return Banner
     */
    public function setClickLink($clickLink)
    {
        $this->clickLink = $clickLink;
        return $this;
    }
}
