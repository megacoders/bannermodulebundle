<?php

namespace Megacoders\BannerModuleBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Megacoders\BannerModuleBundle\Entity\Banner;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BannerDoctrineListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BannerDoctrineListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $object = $args->getEntity();

        if ($object instanceof Banner) {
            $this->removeBannerImageFileByPublicPath($object->getImage());
        }
    }

    /**
     * @param string $publicPath
     */
    private function removeBannerImageFileByPublicPath($publicPath)
    {
        $uploadDirectory = $this->container->getParameter('megacoders_banner.upload_directory');

        if (!$publicPath) {
            return;
        }

        $fileInfo = pathinfo($publicPath);
        $uploadPath = $uploadDirectory .'/' .$fileInfo['basename'];

        if (is_file($uploadPath)) {
            unlink($uploadPath);
        }
    }
}
