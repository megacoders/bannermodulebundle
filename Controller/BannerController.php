<?php
namespace Megacoders\BannerModuleBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Megacoders\BannerModuleBundle\Entity\Banner;
use Megacoders\PageBundle\Controller\Module\BaseModuleController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Router;

class BannerController extends BaseModuleController
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BannerController constructor.
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->entityManager = $this->get('doctrine.orm.entity_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function configureServiceRoutes()
    {
        $collection = new RouteCollection();

        /** Click on banner */
        $collection->add(
            '_service_banner_click',
            new Route(
                '_service/banner/click/{id}',
                ['_controller' => 'MegacodersBannerModuleBundle:Banner:clickService'],
                ['id' => '\d+']
            )
        );

        return $collection;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->listAction($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        $banners = $this->getBannersList();
        $this->prepareBanners($banners);

        return $this->render('list', [
            'banners' => $banners
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function randomAction(Request $request)
    {
        $banner = $this->getRandomBanner();
        $this->prepareBanners($banner);

        return $this->render('random', [
            'banner' => $banner
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function clickServiceAction(Request $request)
    {
        $queryBuilder = $this->getQueryBuilder();
        $expr = $queryBuilder->expr();

        /** @var Banner $banner */
        $banner = $queryBuilder
            ->where($expr->eq('b.id', $request->get('id')))
            ->getQuery()
                ->getOneOrNullResult()
        ;

        $isDebug = $this->get('kernel')->isDebug();

        if (!$isDebug) {
            $banner->incrementNumberOfClicks();
        }

        $this->entityManager->flush($banner);

        return new RedirectResponse($banner->getLink());
    }

    /**
     * @return QueryBuilder
     */
    protected function getQueryBuilder()
    {
        /** @var EntityRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Banner::class);
        $queryBuilder = $repository->createQueryBuilder('b', 'b.id');
        $expr = $queryBuilder->expr();
        $nowStrLiteral = $expr->literal((new \DateTime())->format('Y-m-d H:i:s'));

        $queryBuilder
            ->where($expr->eq('b.active', $expr->literal(true)))
            ->andWhere($expr->lte('b.startDate', $nowStrLiteral))
            ->andWhere($expr->gte('b.endDate', $nowStrLiteral))
        ;

        return $queryBuilder;
    }

    /**
     * @return Banner[]
     */
    protected function getBannersList()
    {
        $queryBuilder = $this->getQueryBuilder();
        $expr = $queryBuilder->expr();

        $queryBuilder
            ->andWhere($expr->eq('b.group', $expr->literal($this->getModuleParameter('group_id'))))
            ->orderBy('b.sortOrder', 'ASC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return Banner|null
     */
    protected function getRandomBanner()
    {
        $queryBuilder = $this->getQueryBuilder();
        $expr = $queryBuilder->expr();

        $queryBuilder
            ->select('count(b.id)')
            ->andWhere($expr->eq('b.group', $expr->literal($this->getModuleParameter('group_id'))))
        ;

        $bannersCount = $queryBuilder->getQuery()->getSingleScalarResult();

        if ($bannersCount == 0) {
            return null;
        }

        $banner = $queryBuilder
            ->select('b')
            ->setFirstResult(rand(0, $bannersCount - 1))
            ->setMaxResults(1)
            ->getQuery()
                ->getOneOrNullResult()
        ;

        return $banner;
    }

    /**
     * @param Banner|Banner[] $banners
     */
    protected function prepareBanners($banners)
    {
        if ($banners === null) {
            return;
        }

        if (!is_array($banners)) {
            $banners = [$banners];
        }

        /** @var Router $router */
        $router = $this->get('router');
        $isDebug = $this->get('kernel')->isDebug();

        foreach ($banners as $banner) {
            $clickLink = $router->generate('_service_banner_click', ['id' => $banner->getId()]);

            if (!$isDebug) {
                $banner->incrementNumberOfViews();
            }

            $banner->setClickLink($clickLink);
        }

        $this->entityManager->flush($banners);
    }
}
